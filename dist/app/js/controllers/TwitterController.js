'use strict';
(function(){
  app.controller('TwitterController', ['$http', '$location','$scope', function TwitterController($http, $location, $scope) {
    $scope.characterLimit = 140; // Characters limit for the tweet. Twitter has 140 characters.
    $scope.userDisplayLimit = 6; // Determines how many matched twitter screenname to display
    $scope.checkScreenName = false; // Initially result for screenname is set to hide

    var getUsers = function() {
      return $http.get($location.absUrl() + 'twitter/user/search?username=' + $scope.lookupScreenName)
        .then(function(response) {
          return response.data;
        });
    };

    var onResultComplete = function(data){
      $scope.result = data.users;
    };

    var onError = function(reason) {
      $scope.error = reason;
    };

    $scope.trackTwitterTextArea = function() {
      findCurrentIndexWord();
    };

    function findCurrentIndexWord() {
      var twitter_text_area = findTwitterTextAreaElement();
      twitter_text_area.focus();
      $scope.currentCursorPosition = getCurrentCursorPosition(twitter_text_area);
      var getCurrentWord = findCurrentWordOfCursorPosition($scope.currentCursorPosition);
      var isTwitterScreenName = checkIfWordStartsWithAt(getCurrentWord);
      if(isTwitterScreenName === true ) {
        $scope.lookupScreenName = stripAtFromScreenName(getCurrentWord);
        if ($scope.lookupScreenName.length >= 2) {
          $scope.checkScreenName = true;
          getUsers().then(onResultComplete, onError);
        }
        else {
          $scope.checkScreenName = false;
        }
      }
      else {
        $scope.checkScreenName = false;
      }
    }

    function findTwitterTextAreaElement() {
      return document.getElementById('twitter_text_area');
    }

    function checkIfWordStartsWithAt(word) {
      return (word.substr(0,1) === '@');
    }

    function stripAtFromScreenName(lookupScreenName) {
      return lookupScreenName.substr(1, (lookupScreenName.length - 1));
    }

    function replaceWordIndex() {
      var wordToReplace = $scope.twitter_tweet_input.substr($scope.currentCursorPosition - ($scope.lookupScreenName.length + 1), $scope.currentCursorPosition);
      $scope.selectedScreenName = "@" + $scope.selectedScreenName + " ";
      if (checkIfLookupNameIsAtTheFirstIndex()) {
        $scope.twitter_tweet_input = $scope.selectedScreenName;
        $scope.checkScreenName = false;
        findTwitterTextAreaElement().focus();
      }
      else {
        var finalResultString = $scope.twitter_tweet_input.substr(0, $scope.currentCursorPosition - ($scope.lookupScreenName.length + 1)) + $scope.selectedScreenName + $scope.twitter_tweet_input.substr(($scope.currentCursorPosition - ($scope.lookupScreenName.length + 1) + wordToReplace.length));
        $scope.twitter_tweet_input = finalResultString;
        $scope.checkScreenName = false;
        findTwitterTextAreaElement().focus();
      }
    }

    function checkIfLookupNameIsAtTheFirstIndex() {
      return  (($scope.currentCursorPosition - ($scope.lookupScreenName.length + 1)) === 0);
    }

    $scope.selectedUsername = function(selectedScreenName) {
      $scope.selectedScreenName = selectedScreenName;
      replaceWordIndex($scope.currentCursorPosition);
    };

    function findCurrentWordOfCursorPosition(currentCursorPosition) {
      if (typeof currentCursorPosition != "undefined") {
        var lastSpaceIndexPosition = ($scope.twitter_tweet_input).substr(0,currentCursorPosition).lastIndexOf(" ");
        if (lastSpaceIndexPosition == -1)
          return ($scope.twitter_tweet_input).substr(0,currentCursorPosition);
        else
          return (($scope.twitter_tweet_input).substr(lastSpaceIndexPosition + 1, currentCursorPosition));
      }
    }

    function getCurrentCursorPosition (element) {
      var cursorPosition = 0;
      // IE Support
      if (document.selection) {
        //element.focus ();
        var selected = document.selection.createRange();
        Sel.moveStart ('character', - element.value.length);
        cursorPosition = Sel.text.length;
      }
      // Firefox support
      else if (element.selectionStart || element.selectionStart == '0')
        cursorPosition = element.selectionStart;

      return (cursorPosition);
    }
  }]);

})();